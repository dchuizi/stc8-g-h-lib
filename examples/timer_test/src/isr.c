#include "isr.h"
#include "config.h"

INTERRUPT(timer0_isr, TIMER0_VECTOR){
    P67 = !P67;
}
INTERRUPT(timer1_isr, TIMER1_VECTOR) {
    P66 = !P66;
}
INTERRUPT(timer2_isr, TIMER2_VECTOR){
    P65 = !P65;
}
INTERRUPT(timer3_isr, TIMER3_VECTOR){
    P64 = !P64;
}
INTERRUPT(timer4_isr, TIMER4_VECTOR){
    P63 = !P63;
}