/*---------------------------------------------------------------------*/
/* --- STC MCU Limited ------------------------------------------------*/
/* --- STC 1T Series MCU Demo Programme -------------------------------*/
/* --- Mobile: (86)13922805190 ----------------------------------------*/
/* --- Fax: 86-0513-55012956,55012947,55012969 ------------------------*/
/* --- Tel: 86-0513-55012928,55012929,55012966 ------------------------*/
/* --- Web: www.STCAI.com ---------------------------------------------*/
/* --- Web: www.STCMCUDATA.com  ---------------------------------------*/
/* --- BBS: www.STCAIMCU.com  -----------------------------------------*/
/* --- QQ:  800003751 -------------------------------------------------*/
/* 如果要在程序中使用此代码,请在程序中注明使用了STC的资料及程序        */
/*---------------------------------------------------------------------*/

/*************  功能说明    **************

本例程基于STC8H8K64U为主控芯片的实验箱9进行编写测试，STC8G、STC8H系列芯片可通用参考.

本程序演示5个定时器的使用, 本例程均使用16位自动重装.

定时器0做16位自动重装, 中断频率为1000HZ，中断函数从P6.7取反输出500HZ方波信号.

定时器1做16位自动重装, 中断频率为2000HZ，中断函数从P6.6取反输出1000HZ方波信号.

定时器2做16位自动重装, 中断频率为3000HZ，中断函数从P6.5取反输出1500HZ方波信号.

定时器3做16位自动重装, 中断频率为4000HZ，中断函数从P6.4取反输出2000HZ方波信号.

定时器4做16位自动重装, 中断频率为5000HZ，中断函数从P6.3取反输出2500HZ方波信号.

下载时, 选择时钟 24MHZ (用户可自行修改频率).

******************************************/

#include "STC8G_H_Timer.h"
#include "STC8G_H_GPIO.h"
#include "isr.h"


void GPIO_Init(){
    GPIO_InitTypeDef initTypeDef;
    initTypeDef.Mode = GPIO_OUT_OD;
    initTypeDef.Pin = GPIO_Pin_4;
    GPIO_Inilize(GPIO_P0, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_5;
    GPIO_Inilize(GPIO_P0, &initTypeDef); //p04,p05 设置为高阻

    initTypeDef.Pin = GPIO_Pin_4;
    GPIO_Inilize(GPIO_P1, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_5;
    GPIO_Inilize(GPIO_P1, &initTypeDef);

    initTypeDef.Pin = GPIO_Pin_2;
    GPIO_Inilize(GPIO_P2, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_3;
    GPIO_Inilize(GPIO_P2, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_4;
    GPIO_Inilize(GPIO_P2, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_5;
    GPIO_Inilize(GPIO_P2, &initTypeDef);

    initTypeDef.Pin = GPIO_Pin_4;
    GPIO_Inilize(GPIO_P3, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_6;
    GPIO_Inilize(GPIO_P3, &initTypeDef);

    initTypeDef.Pin = GPIO_Pin_2;
    GPIO_Inilize(GPIO_P4, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_3;
    GPIO_Inilize(GPIO_P4, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_4;
    GPIO_Inilize(GPIO_P4, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_5;
    GPIO_Inilize(GPIO_P4, &initTypeDef);

    initTypeDef.Pin = GPIO_Pin_2;
    GPIO_Inilize(GPIO_P5, &initTypeDef);
    initTypeDef.Pin = GPIO_Pin_3;
    GPIO_Inilize(GPIO_P5, &initTypeDef);

    initTypeDef.Pin = GPIO_Pin_All;
    GPIO_Inilize(GPIO_P6, &initTypeDef);

    initTypeDef.Mode = GPIO_PullUp;
    initTypeDef.Pin = GPIO_Pin_All;
    GPIO_Inilize(GPIO_P7, &initTypeDef);

    initTypeDef.Pin = GPIO_Pin_0;
    GPIO_Inilize(GPIO_P4, &initTypeDef);

    // P0M1 = 0x30;   P0M0 = 0x30;   //设置P0.4、P0.5为漏极开路(实验箱加了上拉电阻到3.3V)
    // P1M1 = 0x30;   P1M0 = 0x30;   //设置P1.4、P1.5为漏极开路(实验箱加了上拉电阻到3.3V)
    // P2M1 = 0x3c;   P2M0 = 0x3c;   //设置P2.2~P2.5为漏极开路(实验箱加了上拉电阻到3.3V)
    // P3M1 = 0x50;   P3M0 = 0x50;   //设置P3.4、P3.6为漏极开路(实验箱加了上拉电阻到3.3V)
    // P4M1 = 0x3c;   P4M0 = 0x3c;   //设置P4.2~P4.5为漏极开路(实验箱加了上拉电阻到3.3V)
    // P5M1 = 0x0c;   P5M0 = 0x0c;   //设置P5.2、P5.3为漏极开路(实验箱加了上拉电阻到3.3V)
    // P6M1 = 0xff;   P6M0 = 0xff;   //设置为漏极开路(实验箱加了上拉电阻到3.3V)
    // P7M1 = 0x00;   P7M0 = 0x00;   //设置为准双向口
}

void Timer_config(void)
{
	TIM_InitTypeDef		TIM_InitStructure;						//结构定义
	TIM_InitStructure.TIM_Mode      = TIM_16BitAutoReload;	//指定工作模式,   TIM_16BitAutoReload,TIM_16Bit,TIM_8BitAutoReload,TIM_16BitAutoReloadNoMask
	TIM_InitStructure.TIM_Priority    = Priority_0;			//指定中断优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
	TIM_InitStructure.TIM_Interrupt = ENABLE;					//中断是否允许,   ENABLE或DISABLE
	TIM_InitStructure.TIM_ClkSource = TIM_CLOCK_1T;		//指定时钟源,     TIM_CLOCK_1T,TIM_CLOCK_12T,TIM_CLOCK_Ext
	TIM_InitStructure.TIM_ClkOut    = DISABLE;				//是否输出高速脉冲, ENABLE或DISABLE
	TIM_InitStructure.TIM_Value     = 65536UL - (MAIN_Fosc / 1000000UL);		//初值,
	TIM_InitStructure.TIM_Run       = ENABLE;					//是否初始化后启动定时器, ENABLE或DISABLE
	Timer_Inilize(Timer0,&TIM_InitStructure);					//初始化Timer0	  Timer0,Timer1,Timer2,Timer3,Timer4
	
	TIM_InitStructure.TIM_Mode      = TIM_16BitAutoReload;	//指定工作模式,   TIM_16BitAutoReload,TIM_16Bit,TIM_8BitAutoReload,TIM_16BitAutoReloadNoMask
	TIM_InitStructure.TIM_Priority    = Priority_0;			//指定中断优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
	TIM_InitStructure.TIM_Interrupt = ENABLE;					//中断是否允许,   ENABLE或DISABLE
	TIM_InitStructure.TIM_ClkSource = TIM_CLOCK_1T;		//指定时钟源, TIM_CLOCK_1T,TIM_CLOCK_12T,TIM_CLOCK_Ext
	TIM_InitStructure.TIM_ClkOut    = DISABLE;				//是否输出高速脉冲, ENABLE或DISABLE
	TIM_InitStructure.TIM_Value     = 65536UL - (MAIN_Fosc / 10000);			//初值,
	TIM_InitStructure.TIM_Run       = ENABLE;					//是否初始化后启动定时器, ENABLE或DISABLE
	Timer_Inilize(Timer1,&TIM_InitStructure);					//初始化Timer1	  Timer0,Timer1,Timer2,Timer3,Timer4

	TIM_InitStructure.TIM_Interrupt = ENABLE;					//中断是否允许,   ENABLE或DISABLE. (注意: Timer2固定为16位自动重装, 中断固定为低优先级)
	TIM_InitStructure.TIM_ClkSource = TIM_CLOCK_1T;		//指定时钟源,     TIM_CLOCK_1T,TIM_CLOCK_12T,TIM_CLOCK_Ext
	TIM_InitStructure.TIM_ClkOut    = DISABLE;				//是否输出高速脉冲, ENABLE或DISABLE
	TIM_InitStructure.TIM_Value     = 65536UL - (MAIN_Fosc / 1000);				//初值
	TIM_InitStructure.TIM_Run       = ENABLE;					//是否初始化后启动定时器, ENABLE或DISABLE
	Timer_Inilize(Timer2,&TIM_InitStructure);					//初始化Timer2	  Timer0,Timer1,Timer2,Timer3,Timer4

	TIM_InitStructure.TIM_Interrupt = ENABLE;					//中断是否允许,   ENABLE或DISABLE. (注意: Timer2固定为16位自动重装, 中断固定为低优先级)
	TIM_InitStructure.TIM_ClkSource = TIM_CLOCK_12T;	//指定时钟源,     TIM_CLOCK_1T,TIM_CLOCK_12T,TIM_CLOCK_Ext
	TIM_InitStructure.TIM_ClkOut    = ENABLE;					//是否输出高速脉冲, ENABLE或DISABLE
	TIM_InitStructure.TIM_Value     = 65536UL - (MAIN_Fosc / (100*12));		//初值
	TIM_InitStructure.TIM_Run       = ENABLE;					//是否初始化后启动定时器, ENABLE或DISABLE
	Timer_Inilize(Timer3,&TIM_InitStructure);					//初始化Timer3	  Timer0,Timer1,Timer2,Timer3,Timer4

	TIM_InitStructure.TIM_Interrupt = ENABLE;					//中断是否允许,   ENABLE或DISABLE. (注意: Timer2固定为16位自动重装, 中断固定为低优先级)
	TIM_InitStructure.TIM_ClkSource = TIM_CLOCK_12T;	//指定时钟源,     TIM_CLOCK_1T,TIM_CLOCK_12T,TIM_CLOCK_Ext
	TIM_InitStructure.TIM_ClkOut    = ENABLE;					//是否输出高速脉冲, ENABLE或DISABLE
	TIM_InitStructure.TIM_Value     = 65536UL - (MAIN_Fosc / (50*12));		//初值
	TIM_InitStructure.TIM_Run       = ENABLE;					//是否初始化后启动定时器, ENABLE或DISABLE
	Timer_Inilize(Timer4,&TIM_InitStructure);					//初始化Timer4	  Timer0,Timer1,Timer2,Timer3,Timer4
}


//========================================================================
// 函数: void main(void)
// 描述: 主函数.
// 参数: none.
// 返回: none.
// 版本: V1.0, 2015-1-12
//========================================================================
void main(void)
{
    P_SW2 |= 0x80;  //扩展寄存器(XFR)访问使能

    GPIO_Init();
    Timer_config();

    EA = 1;         //打开总中断
    P40 = 0;		//打开LED电源

    while (1)
    {

    }
}

