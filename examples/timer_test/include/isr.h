#include "config.h"
#include <STC8G_H_Timer.h>

INTERRUPT(timer0_isr, TIMER0_VECTOR);
INTERRUPT(timer1_isr, TIMER1_VECTOR);
INTERRUPT(timer2_isr, TIMER2_VECTOR);
INTERRUPT(timer3_isr, TIMER3_VECTOR);
INTERRUPT(timer4_isr, TIMER4_VECTOR);