/*---------------------------------------------------------------------*/
/* --- STC MCU Limited ------------------------------------------------*/
/* --- STC 1T Series MCU Demo Programme -------------------------------*/
/* --- Mobile: (86)13922805190 ----------------------------------------*/
/* --- Fax: 86-0513-55012956,55012947,55012969 ------------------------*/
/* --- Tel: 86-0513-55012928,55012929,55012966 ------------------------*/
/* --- Web: www.STCAI.com ---------------------------------------------*/
/* --- Web: www.STCMCUDATA.com  ---------------------------------------*/
/* --- BBS: www.STCAIMCU.com  -----------------------------------------*/
/* --- QQ:  800003751 -------------------------------------------------*/
/* 如果要在程序中使用此代码,请在程序中注明使用了STC的资料及程序        */
/*---------------------------------------------------------------------*/


/*************  本程序功能说明  **************

本例程基于STC8H8K64U为主控芯片的实验箱9进行编写测试，STC8G、STC8H系列芯片可通用参考.

用STC的MCU的IO方式驱动8位数码管。

显示效果为: 显示秒计数, 5秒后不喂狗, 等复位.

下载时, 选择时钟 24MHZ (用户可自行修改频率).

******************************************/

#include <STC8G_H_GPIO.h>
#include <STC8G_H_Delay.h>
#include <STC8G_H_WDT.h>

#define DIS_DOT     0x20
#define DIS_BLACK   0x10
#define DIS_        0x11

/****************************** 用户定义宏 ***********************************/

#define Timer0_Reload   (65536UL -(MAIN_Fosc / 1000))       //Timer 0 中断频率, 1000次/秒

/*****************************************************************************/


/*************  本地常量声明    **************/
u8 __code t_display[]={                       //标准字库
//   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
    0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F,0x77,0x7C,0x39,0x5E,0x79,0x71,
//black  -     H    J    K    L    N    o   P    U     t    G    Q    r   M    y
    0x00,0x40,0x76,0x1E,0x70,0x38,0x37,0x5C,0x73,0x3E,0x78,0x3d,0x67,0x50,0x37,0x6e,
    0xBF,0x86,0xDB,0xCF,0xE6,0xED,0xFD,0x87,0xFF,0xEF,0x46};    //0. 1. 2. 3. 4. 5. 6. 7. 8. 9. -1

u8 __code T_COM[]={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};      //位码


/*************  本地变量声明    **************/

u8  LED8[8];        //显示缓冲
u8  display_index;  //显示位索引

u16 ms_cnt;
u8  tes_cnt;    //测试用的计数变量

void GPIO_Init();
void DisplayScan(void);

/********************** 主函数 ************************/
void main(void)
{
    u8  i;
    
    P_SW2 |= 0x80;  //扩展寄存器(XFR)访问使能

    GPIO_Init();
    WDT_InitTypeDef wdtInit;
    wdtInit.WDT_Enable = ENABLE;
    wdtInit.WDT_IDLE_Mode = WDT_IDLE_RUN;
    wdtInit.WDT_PS = WDT_SCALE_16;
    WDT_Inilize(&wdtInit);

    RSTFLAG |= 0x04;   //设置看门狗复位需要检测P3.2的状态，否则看门狗复位后进入USB下载模式

    display_index = 0;
    for(i=0; i<8; i++)  LED8[i] = DIS_BLACK;    //全部消隐
    
    tes_cnt = 0;
    ms_cnt = 0;
    LED8[7] = ms_cnt;

    while(1)
    {
        delay_ms(1);    //延时1ms
        DisplayScan();
        if(tes_cnt <= 5)    //5秒后不喂狗, 将复位,
        {
            WDT_Clear();
        }
            

        if(++ms_cnt >= 1000)
        {
            ms_cnt = 0;
            tes_cnt++;
            LED8[7] = tes_cnt;
        }

    }
} 

/**********************************************/
//========================================================================
// 函数: void GPIO_Init (void)
// 描述: 端口初始化函数.
// 参数: none.
// 返回: none.
//========================================================================
void GPIO_Init()
{
    GPIO_InitTypeDef initTypeDef;
    initTypeDef.Mode = GPIO_OUT_OD;
    initTypeDef.Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Inilize(GPIO_P0, &initTypeDef); // 设置P0.4、P0.5为漏极开路(实验箱加了上拉电阻到3.3V)
    GPIO_Inilize(GPIO_P1, &initTypeDef); // 设置P1.4、P1.5为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Inilize(GPIO_P2, &initTypeDef); // 设置P2.2~P2.5为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6;
    GPIO_Inilize(GPIO_P3, &initTypeDef); // 设置P3.4、P3.6为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Inilize(GPIO_P4, &initTypeDef); // 设置P4.2~P4.5为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_Inilize(GPIO_P5, &initTypeDef); // 设置P5.2、P5.3为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_All;
    GPIO_Inilize(GPIO_P6, &initTypeDef); // 设置为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Mode = GPIO_PullUp;
    initTypeDef.Pin = GPIO_Pin_All;
    GPIO_Inilize(GPIO_P7, &initTypeDef); // 设置为准双向口
}



/********************** 显示扫描函数 ************************/
void DisplayScan(void)
{   
    P7 = ~T_COM[7-display_index];
    P6 = ~t_display[LED8[display_index]];
    if(++display_index >= 8)    display_index = 0;  //8位结束回0
}

