#include <STC8G_H_GPIO.h>

void Delay1ms()		//@35MHz 
{
	unsigned char i, j;

	NOP();
	NOP();
	i = 46;

	
	j = 113;
	do
	{
		while (--j);
	} while (--i);
}

/* n毫秒延时函数 参数给几 就延时几毫秒 */
void delay_ms(unsigned int ms) 
{
	while(ms--)
	{
		Delay1ms();
	}
}

/* 发光二极管定义 */
#define LED_R  P05    // 红色LED
#define LED_Y  P06    // 黄色LED
#define LED_G  P07    // 绿色LED

void main(void) {
    GPIO_InitTypeDef	GPIO_InitStructure;		//结构定义
	GPIO_InitStructure.Pin  = GPIO_Pin_All;		//指定要初始化的IO,
	GPIO_InitStructure.Mode = GPIO_PullUp;		//指定IO的输入或输出方式,GPIO_PullUp,GPIO_HighZ,GPIO_OUT_OD,GPIO_OUT_PP
	GPIO_Inilize(GPIO_P0,&GPIO_InitStructure);	//初始化
    while (1)
    {
        LED_G = 0;     // 点亮所有灯
		LED_R = 0;
		LED_Y = 0;
		delay_ms(500); // 等待500ms
		LED_G = 1;     // 关闭所有灯
		LED_R = 1;
		LED_Y = 1;
		delay_ms(500); // 等待500ms
    }
    
}