/*---------------------------------------------------------------------*/
/* --- STC MCU Limited ------------------------------------------------*/
/* --- STC 1T Series MCU Demo Programme -------------------------------*/
/* --- Mobile: (86)13922805190 ----------------------------------------*/
/* --- Fax: 86-0513-55012956,55012947,55012969 ------------------------*/
/* --- Tel: 86-0513-55012928,55012929,55012966 ------------------------*/
/* --- Web: www.STCAI.com ---------------------------------------------*/
/* --- Web: www.STCMCUDATA.com  ---------------------------------------*/
/* --- BBS: www.STCAIMCU.com  -----------------------------------------*/
/* --- QQ:  800003751 -------------------------------------------------*/
/* 如果要在程序中使用此代码,请在程序中注明使用了STC的资料及程序        */
/*---------------------------------------------------------------------*/


/*************  本程序功能说明  **************

本例程基于STC8H8K64U为主控芯片的实验箱9进行编写测试，STC8G、STC8H系列芯片可通用参考.

用STC的MCU的IO方式驱动8位数码管。

显示效果为: 数码时钟.

使用Timer0的16位自动重装来产生1ms节拍,程序运行于这个节拍下,用户修改MCU主时钟频率时,自动定时于1ms.

左边4位LED显示时间(小时,分钟), 右边最后两位显示按键值.

行列扫描按键键码为17~32, 实验箱9按键矩阵为2*4, 按键键码从25~32.

按键只支持单键按下, 不支持多键同时按下, 那样将会有不可预知的结果.

键按下超过1秒后,将以10键/秒的速度提供重键输出. 用户只需要检测KeyCode是否非0来判断键是否按下.

调整时间键:
键码17: 小时+.
键码18: 小时-.
键码19: 分钟+.
键码20: 分钟-.

下载时, 选择时钟 24MHZ (用户可自行修改频率).

******************************************/

#include <STC8G_H_GPIO.h>
#include <STC8G_H_Timer.h>
#include <STC8G_H_Delay.h>


#define Timer0_Reload   (65536UL -(MAIN_Fosc / 1000))       //Timer 0 中断频率, 1000次/秒

#define DIS_DOT     0x20
#define DIS_BLACK   0x10
#define DIS_        0x11


u8 __code t_display[]={                       //标准字库
//   0    1    2    3    4    5    6    7    8    9    A    B    C    D    E    F
    0x3F,0x06,0x5B,0x4F,0x66,0x6D,0x7D,0x07,0x7F,0x6F,0x77,0x7C,0x39,0x5E,0x79,0x71,
//black  -     H    J    K    L    N    o   P    U     t    G    Q    r   M    y
    0x00,0x40,0x76,0x1E,0x70,0x38,0x37,0x5C,0x73,0x3E,0x78,0x3d,0x67,0x50,0x37,0x6e,
    0xBF,0x86,0xDB,0xCF,0xE6,0xED,0xFD,0x87,0xFF,0xEF,0x46};    //0. 1. 2. 3. 4. 5. 6. 7. 8. 9. -1

u8 __code T_COM[]={0x01,0x02,0x04,0x08,0x10,0x20,0x40,0x80};      //位码


u8  LED8[8];        //显示缓冲
u8  display_index;  //显示位索引
u8  B_1ms;          //1ms标志

u8  KeyCode;    //给用户使用的键码, 1~16有效
u8  cnt10ms;

u8 IO_KeyState, IO_KeyState1, IO_KeyHoldCnt;    //行列键盘变量
u8  cnt50ms;

u8  hour,minute,second; //RTC变量
u16 msecond;

void IO_KeyScan(void);   //50ms call

INTERRUPT(timer0_int, TIMER0_VECTOR);


/********************** 显示时钟函数 ************************/
void DisplayRTC(void)
{
    if(hour >= 10) {
        LED8[0] = hour / 10;
    }
    else 
    {
        LED8[0] = DIS_BLACK;
    }           
    LED8[1] = hour % 10;
    LED8[2] = minute / 10;
    LED8[3] = minute % 10;
    if(msecond >= 500) {
        LED8[1] |= DIS_DOT; //小时后的小数点做秒闪
    }
}

/********************** RTC演示函数 ************************/
void RTC(void)
{
    if(++second >= 60)
    {
        second = 0;
        if(++minute >= 60)
        {
            minute = 0;
            if(++hour >= 24)    hour = 0;
        }
    }
}

//========================================================================
// 函数: void GPIO_Init (void)
// 描述: 端口初始化函数.
// 参数: none.
// 返回: none.
//========================================================================
void GPIO_Init();

//========================================================================
// 函数: void TIMER_Init (void)
// 描述: 定时器初始化函数.
// 参数: none.
// 返回: none.
//========================================================================
void TIMER_Init();

/**********************************************/
void main(void)
{
    u8  i;
    
    P_SW2 |= 0x80;  //扩展寄存器(XFR)访问使能
    GPIO_Init(); // 初始化GPIO
    display_index = 0;
    TIMER_Init(); // 初始化定时器
    EA = 1;     //打开总中断
    
    for(i=0; i<8; i++)  LED8[i] = 0x10; //上电消隐

    hour   = 12;    //初始化时间值
    minute = 0;
    second = 0;
    DisplayRTC();

    KeyCode = 0;    //给用户使用的键码, 1~16有效
    cnt10ms = 0;

    IO_KeyState = 0;
    IO_KeyState1 = 0;
    IO_KeyHoldCnt = 0;
    cnt50ms = 0;
    
    while(1)
    {
        if(B_1ms)   //1ms到
        {
            B_1ms = 0;
            if(++msecond >= 1000)   //1秒到
            {
                msecond = 0;
                RTC();
                DisplayRTC();
            }
            if(msecond == 500)  DisplayRTC();   //小时后的小数点做秒闪


            if(++cnt50ms >= 50)     //50ms扫描一次行列键盘
            {
                cnt50ms = 0;
                IO_KeyScan();
            }
            
            if(KeyCode > 0)     //有键按下
            {
                LED8[6] = KeyCode / 10; //显示键码
                LED8[7] = KeyCode % 10; //显示键码

                if(KeyCode == 25)   //hour +1
                {
                    if(++hour >= 24)    hour = 0;
                    DisplayRTC();
                }
                if(KeyCode == 26)   //hour -1
                {
                    if(--hour >= 24)    hour = 23;
                    DisplayRTC();
                }
                if(KeyCode == 27)   //minute +1
                {
                    second = 0;
                    if(++minute >= 60)  minute = 0;
                    DisplayRTC();
                }
                if(KeyCode == 28)   //minute -1
                {
                    second = 0;
                    if(--minute >= 60)  minute = 59;
                    DisplayRTC();
                }
                KeyCode = 0;
            }
        }
    }
} 
/**********************************************/


/*****************************************************
    行列键扫描程序
    使用XY查找4x4键的方法，只能单键，速度快

   Y     P04      P05      P06      P07
          |        |        |        |
X         |        |        |        |
P00 ---- K00 ---- K01 ---- K02 ---- K03 ----
          |        |        |        |
P01 ---- K04 ---- K05 ---- K06 ---- K07 ----
          |        |        |        |
P02 ---- K08 ---- K09 ---- K10 ---- K11 ----
          |        |        |        |
P03 ---- K12 ---- K13 ---- K14 ---- K15 ----
          |        |        |        |
******************************************************/


u8 __code T_KeyTable[16] = {0,1,2,0,3,0,0,0,4,0,0,0,0,0,0,0};

void IO_KeyDelay(void)
{
    u8 i;
    i = 60;
    while(--i)  ;
}

void IO_KeyScan(void)    //50ms call
{
    u8  j;

    j = IO_KeyState1;   //保存上一次状态

    P0 = 0xf0;  //X低，读Y
    IO_KeyDelay();
    IO_KeyState1 = P0 & 0xf0;

    P0 = 0x0f;  //Y低，读X
    IO_KeyDelay();
    IO_KeyState1 |= (P0 & 0x0f);
    IO_KeyState1 ^= 0xff;   //取反
    
    if(j == IO_KeyState1)   //连续两次读相等
    {
        j = IO_KeyState;
        IO_KeyState = IO_KeyState1;
        if(IO_KeyState != 0)    //有键按下
        {
            F0 = 0;
            if(j == 0)  F0 = 1; //第一次按下
            else if(j == IO_KeyState)
            {
                if(++IO_KeyHoldCnt >= 20)   //1秒后重键
                {
                    IO_KeyHoldCnt = 18;
                    F0 = 1;
                }
            }
            if(F0)
            {
                j = T_KeyTable[IO_KeyState >> 4];
                if((j != 0) && (T_KeyTable[IO_KeyState& 0x0f] != 0)) 
                    KeyCode = (j - 1) * 4 + T_KeyTable[IO_KeyState & 0x0f] + 16;    //计算键码，17~32
            }
        }
        else    IO_KeyHoldCnt = 0;
    }
    P0 = 0xff;
}


/********************** 显示扫描函数 ************************/
void DisplayScan(void)
{   
    P7 = ~T_COM[7-display_index];
    P6 = ~t_display[LED8[display_index]];
    if(++display_index >= 8)    display_index = 0;  //8位结束回0
}


/********************** Timer0 1ms中断函数 ************************/
INTERRUPT(timer0_int, TIMER0_VECTOR)
{
    DisplayScan();  //1ms扫描显示一位
    B_1ms = 1;      //1ms标志
}

void GPIO_Init()
{
    GPIO_InitTypeDef initTypeDef;
    initTypeDef.Mode = GPIO_PullUp;
    initTypeDef.Pin = GPIO_Pin_All;
    GPIO_Inilize(GPIO_P0, &initTypeDef); // 设置为准双向口
    initTypeDef.Mode = GPIO_OUT_OD;
    initTypeDef.Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Inilize(GPIO_P0, &initTypeDef); // 设置P0.4、P0.5为漏极开路(实验箱加了上拉电阻到3.3V)
    GPIO_Inilize(GPIO_P1, &initTypeDef); // 设置P1.4、P1.5为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Inilize(GPIO_P2, &initTypeDef); // 设置P2.2~P2.5为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6;
    GPIO_Inilize(GPIO_P3, &initTypeDef); // 设置P3.4、P3.6为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Inilize(GPIO_P4, &initTypeDef); // 设置P4.2~P4.5为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_Inilize(GPIO_P5, &initTypeDef); // 设置P5.2、P5.3为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_All;
    GPIO_Inilize(GPIO_P6, &initTypeDef); // 设置为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Mode = GPIO_PullUp;
    initTypeDef.Pin = GPIO_Pin_All;
    GPIO_Inilize(GPIO_P7, &initTypeDef); // 设置为准双向口
    
}

void TIMER_Init()
{
    TIM_InitTypeDef timer0;
    timer0.TIM_Mode = TIM_16BitAutoReload;
    timer0.TIM_Interrupt = ENABLE;
    timer0.TIM_Value = Timer0_Reload;
    timer0.TIM_Run = ENABLE;
    timer0.TIM_ClkSource = TIM_CLOCK_1T;

    Timer_Inilize(Timer0, &timer0);
}

