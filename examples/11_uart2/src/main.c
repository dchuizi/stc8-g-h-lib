/*---------------------------------------------------------------------*/
/* --- STC MCU Limited ------------------------------------------------*/
/* --- STC 1T Series MCU Demo Programme -------------------------------*/
/* --- Mobile: (86)13922805190 ----------------------------------------*/
/* --- Fax: 86-0513-55012956,55012947,55012969 ------------------------*/
/* --- Tel: 86-0513-55012928,55012929,55012966 ------------------------*/
/* --- Web: www.STCAI.com ---------------------------------------------*/
/* --- Web: www.STCMCUDATA.com  ---------------------------------------*/
/* --- BBS: www.STCAIMCU.com  -----------------------------------------*/
/* --- QQ:  800003751 -------------------------------------------------*/
/* 如果要在程序中使用此代码,请在程序中注明使用了STC的资料及程序        */
/*---------------------------------------------------------------------*/

/*************  功能说明    **************
本例程基于STC8H8K64U为主控芯片的实验箱9进行编写测试，STC8G、STC8H系列芯片可通用参考.
串口1全双工中断方式收发通讯程序。
通过PC向MCU发送数据, MCU收到后通过串口1把收到的数据原样返回.
用定时器做波特率发生器，建议使用1T模式(除非低波特率用12T)，并选择可被波特率整除的时钟频率，以提高精度。
下载时, 选择时钟 22.1184MHz (用户可自行修改频率).

******************************************/

#include <STC8G_H_GPIO.h>
#include <STC8G_H_UART.h>
#include <STC8G_H_Timer.h>
#include <STC8G_H_Delay.h>


#define     Baudrate1           115200L
#define     UART1_BUF_LENGTH    64

u8 TX1_Cnt;     //发送计数器
u8 RX1_Cnt;     //接收计数器
u8 B_TX1_Busy;  //发送忙标志

INTERRUPT(UART2_int, UART2_VECTOR);
void GPIO_Init();
void UART2_Init();

//========================================================================
// 函数: void main(void)
// 描述: 主函数。
// 参数: none.
// 返回: none.
// 版本: VER1.0
// 日期: 2014-11-28
// 备注: 
//========================================================================
void main(void)
{
    u8 i;
    P_SW2 |= 0x80;  //扩展寄存器(XFR)访问使能

    GPIO_Init();
    UART2_Init();

    EA = 1; //允许总中断

    PrintString2("STC8H8K64U UART2 Test Programme!\r\n");  //UART1发送一个字符串

    while (1)
    {
        delay_ms(1);
        if(COM2.RX_TimeOut > 0) {
            if(--COM2.RX_TimeOut == 0) {
                if(COM2.RX_Cnt > 0) {
                    for (i = 0; i < COM2.RX_Cnt; i++)
                    {
                        TX2_write2buff(RX2_Buffer[i]);
                    }
                }
                COM2.RX_Cnt = 0;
            }
        }
    }
}

//========================================================================
// 函数: void GPIO_Init (void)
// 描述: 端口初始化函数.
// 参数: none.
// 返回: none.
//========================================================================
void GPIO_Init()
{
    GPIO_InitTypeDef initTypeDef;
    initTypeDef.Mode = GPIO_OUT_OD;
    initTypeDef.Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Inilize(GPIO_P0, &initTypeDef); // 设置P0.4、P0.5为漏极开路(实验箱加了上拉电阻到3.3V)
    GPIO_Inilize(GPIO_P1, &initTypeDef); // 设置P1.4、P1.5为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Inilize(GPIO_P2, &initTypeDef); // 设置P2.2~P2.5为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6;
    GPIO_Inilize(GPIO_P3, &initTypeDef); // 设置P3.4、P3.6为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Inilize(GPIO_P4, &initTypeDef); // 设置P4.2~P4.5为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_2 | GPIO_Pin_3;
    GPIO_Inilize(GPIO_P5, &initTypeDef); // 设置P5.2、P5.3为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Pin = GPIO_Pin_All;
    GPIO_Inilize(GPIO_P6, &initTypeDef); // 设置为漏极开路(实验箱加了上拉电阻到3.3V)
    initTypeDef.Mode = GPIO_PullUp;
    initTypeDef.Pin = GPIO_Pin_All;
    GPIO_Inilize(GPIO_P7, &initTypeDef); // 设置为准双向口
    initTypeDef.Pin = GPIO_Pin_6 | GPIO_Pin_7;
    GPIO_Inilize(GPIO_P4, &initTypeDef); // 设置为准双向口
}

//========================================================================
// 函数: void UART2_Init (void)
// 描述: 串口2初始化函数.
// 参数: none.
// 返回: none.
//========================================================================
void UART2_Init(){
    COMx_InitDefine		COMx_InitStructure;					//结构定义
	COMx_InitStructure.UART_Mode      = UART_8bit_BRTx;		//模式,       UART_ShiftRight,UART_8bit_BRTx,UART_9bit,UART_9bit_BRTx
	COMx_InitStructure.UART_BRT_Use   = BRT_Timer2;			//选择波特率发生器,   BRT_Timer1, BRT_Timer2 (注意: 串口2固定使用BRT_Timer2)
	COMx_InitStructure.UART_BaudRate  = 115200ul;			//波特率, 一般 110 ~ 115200
	COMx_InitStructure.UART_RxEnable  = ENABLE;				//接收允许,   ENABLE或DISABLE
	COMx_InitStructure.BaudRateDouble = DISABLE;			//波特率加倍, ENABLE或DISABLE
	COMx_InitStructure.UART_Interrupt = ENABLE;				//中断允许,   ENABLE或DISABLE
	COMx_InitStructure.UART_Priority  = Priority_0;			//指定中断优先级(低到高) Priority_0,Priority_1,Priority_2,Priority_3
	COMx_InitStructure.UART_P_SW      = UART2_SW_P46_P47;	//切换端口,   UART1_SW_P30_P31,UART1_SW_P36_P37,UART1_SW_P16_P17,UART1_SW_P43_P44
	UART_Configuration(UART2, &COMx_InitStructure);		    //初始化串口1 UART1,UART2,UART3,UART4
}

//========================================================================
// 函数: void UART2_int (void) interrupt UART1_VECTOR
// 描述: UART1中断函数。
// 参数: nine.
// 返回: none.
// 版本: VER1.0
// 日期: 2014-11-28
// 备注: 
//========================================================================
INTERRUPT(UART2_int, UART2_VECTOR)
{
    if(RI2)
    {
        CLR_RI2();
        if(COM2.B_RX_OK == 0)
        {
            if(COM2.RX_Cnt >= COM_RX1_Lenth) {
                COM2.RX_Cnt = 0;
            }
            RX2_Buffer[COM2.RX_Cnt++] = S2BUF;
            COM2.RX_TimeOut = TimeOutSet2;
        }
    }

    if(TI2)
    {
        CLR_TI2();
        if(COM2.TX_read != COM2.TX_write) 
        {
            S2BUF = TX2_Buffer[COM2.TX_read];
            if(++COM2.TX_read >= COM_TX2_Lenth)
            {
                COM2.TX_read = 0;
            }
        } else {
            COM2.B_TX_busy = 0;		//标志忙
        }

    }
}

