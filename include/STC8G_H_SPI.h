/*---------------------------------------------------------------------*/
/* --- STC MCU Limited ------------------------------------------------*/
/* --- STC 1T Series MCU Demo Programme -------------------------------*/
/* --- Mobile: (86)13922805190 ----------------------------------------*/
/* --- Fax: 86-0513-55012956,55012947,55012969 ------------------------*/
/* --- Tel: 86-0513-55012928,55012929,55012966 ------------------------*/
/* --- Web: www.STCAI.com ---------------------------------------------*/
/* --- BBS: www.STCAIMCU.com  -----------------------------------------*/
/* --- QQ:  800003751 -------------------------------------------------*/
/* 如果要在程序中使用此代码,请在程序中注明使用了STC的资料及程序            */
/*---------------------------------------------------------------------*/

#ifndef	__STC8G_H_SPI_H
#define	__STC8G_H_SPI_H

#include "config.h"

//========================================================================
//                              寄存器位定义
//========================================================================

//sfr SPSTAT  = 0xCD;	//SPI状态寄存器
//   7       6      5   4   3   2   1   0    	Reset Value
//	SPIF	WCOL	-	-	-	-	-	-
#define	SPIF	0x80		/* SPI传输完成标志。写入1清0。*/
#define	WCOL	0x40		/* SPI写冲突标志。写入1清0。  */

//========================================================================
//                               SPI设置
//========================================================================

//sfr SPCTL  = 0xCE;	SPI控制寄存器
//   7       6       5       4       3       2       1       0    	Reset Value
//	SSIG	SPEN	DORD	MSTR	CPOL	CPHA	SPR1	SPR0		0x00

#define	SPI_SSIG_None()		SPCTL |=  (1<<7)		/* 1: 忽略SS脚	*/
#define	SPI_SSIG_Enable()	SPCTL &= ~(1<<7)		/* 0: SS脚用于决定主从机	*/
#define	SPI_Enable()		SPCTL |=  (1<<6)		/* 1: 允许SPI	*/
#define	SPI_Disable()		SPCTL &= ~(1<<6)		/* 0: 禁止SPI	*/

#define		SPI_SSIG_Set(n)			SPCTL = (SPCTL & ~0x80) | (n << 7)		/* SS引脚功能控制 */
#define		SPI_Start(n)			SPCTL = (SPCTL & ~0x40) | (n << 6)		/* SPI使能控制位 */
#define		SPI_FirstBit_Set(n)		SPCTL = (SPCTL & ~0x20) | (n << 5)		/* 数据发送/接收顺序 MSB/LSB */
#define		SPI_Mode_Set(n)			SPCTL = (SPCTL & ~0x10) | (n << 4)		/* SPI主从模式设置 */
#define		SPI_CPOL_Set(n)			SPCTL = (SPCTL & ~0x08) | (n << 3)		/* SPI时钟极性控制 */
#define		SPI_CPHA_Set(n)			SPCTL = (SPCTL & ~0x04) | (n << 2)		/* SPI时钟相位控制 */
#define		SPI_Clock_Select(n)		SPCTL = (SPCTL & ~0x03) | (n)			/* SPI时钟频率选择 */

#define	SPI_LSB_First()		SPCTL |=  (1<<5)		/* 1: LSB先发	*/
#define	SPI_MSB_First()		SPCTL &= ~(1<<5)		/* 0: MSB先发	*/
#define	SPI_Master()		SPCTL |=  (1<<4)		/* 1: 设为主机	*/
#define	SPI_Slave()			SPCTL &= ~(1<<4)		/* 0: 设为从机	*/
#define	SPI_SCLK_NormalH()	SPCTL |=  (1<<3)		/* 1: 空闲时SCLK为高电平	*/
#define	SPI_SCLK_NormalL()	SPCTL &= ~(1<<3)		/* 0: 空闲时SCLK为低电平	*/
#define	SPI_PhaseH()		SPCTL |=  (1<<2)		/* 1: 	*/
#define	SPI_PhaseL()		SPCTL &= ~(1<<2)		/* 0: 	*/
#define	SPI_Speed(n)		SPCTL = (SPCTL & ~3) | (n)	/*设置速度, 0 -- fosc/4, 1 -- fosc/8, 2 -- fosc/16, 3 -- fosc/32	*/

//sfr SPDAT  = 0xCF; //SPI Data Register                                                     0000,0000
//sfr SPSTAT  = 0xCD;	//SPI状态寄存器

#define		SPI_USE_P12P13P14P15()	P_SW1 &= ~0x0c					/* 将SPI切换到P12(SS) P13(MOSI) P14(MISO) P15(SCLK)(上电默认)。*/
#define		SPI_USE_P22P23P24P25()	P_SW1 = (P_SW1 & ~0x0c) | 0x04	/* 将SPI切换到P22(SS) P23(MOSI) P24(MISO) P25(SCLK)。*/
#define		SPI_USE_P74P75P76P77()	P_SW1 = (P_SW1 & ~0x0c) | 0x08	/* 将SPI切换到P74(SS) P75(MOSI) P76(MISO) P77(SCLK)。*/
#define		SPI_USE_P35P34P33P32()	P_SW1 =  P_SW1 | 0x0C			/* 将SPI切换到P35(SS) P34(MOSI) P33(MISO) P32(SCLK)。*/



//========================================================================
//                              定义声明
//========================================================================

#define	SPI_BUF_LENTH	128
#define	SPI_BUF_type	__xdata

#define  SPI_SS    P12
#define  SPI_MOSI  P13
#define  SPI_MISO  P14
#define  SPI_SCLK  P15

#define  SPI_SS_2    P22
#define  SPI_MOSI_2  P23
#define  SPI_MISO_2  P24
#define  SPI_SCLK_2  P25

#define  SPI_SS_3    P54
#define  SPI_MOSI_3  P40
#define  SPI_MISO_3  P41
#define  SPI_SCLK_3  P43

#define  SPI_SS_4     P35
#define  SPI_MOSI_4   P34
#define  SPI_MISO_4   P33
#define  SPI_SCLK_4   P32

#define	SPI_Mode_Master     1
#define	SPI_Mode_Slave      0
#define	SPI_CPOL_High       1
#define	SPI_CPOL_Low        0
#define	SPI_CPHA_1Edge      0
#define	SPI_CPHA_2Edge      1
#define	SPI_Speed_4         0
#define	SPI_Speed_8         1
#define	SPI_Speed_16        2
#define	SPI_Speed_2         3
#define	SPI_Speed_32        3
#define	SPI_MSB             0
#define	SPI_LSB             1

typedef struct
{
	u8	SPI_Enable;     //ENABLE,DISABLE
	u8	SPI_SSIG;       //ENABLE(ignore SS), DISABLE
	u8	SPI_FirstBit;   //SPI_MSB, SPI_LSB
	u8	SPI_Mode;       //SPI_Mode_Master, SPI_Mode_Slave
	u8	SPI_CPOL;       //SPI_CPOL_High,   SPI_CPOL_Low
	u8	SPI_CPHA;       //SPI_CPHA_1Edge,  SPI_CPHA_2Edge
	u8	SPI_Speed;      //SPI_Speed_4,SPI_Speed_16,SPI_Speed_64,SPI_Speed_2/SPI_Speed_32
} SPI_InitTypeDef;


extern	unsigned char B_SPI_Busy; //发送忙标志
extern	u8 	SPI_RxCnt;
extern	u8 	SPI_RxTimerOut;
extern	u8 	SPI_BUF_type SPI_RxBuffer[SPI_BUF_LENTH];


void	SPI_Init(SPI_InitTypeDef *SPIx);
void	SPI_SetMode(u8 mode);
void	SPI_WriteByte(u8 dat);

#endif

